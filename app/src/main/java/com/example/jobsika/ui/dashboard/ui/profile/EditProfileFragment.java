package com.example.jobsika.ui.dashboard.ui.profile;

import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.example.jobsika.R;
import com.example.jobsika.databinding.FragmentEditProfileBinding;
import com.example.jobsika.databinding.FragmentProfileBinding;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.Objects;

public class EditProfileFragment extends Fragment {

    private EditProfileViewModel mViewModel;
    private FragmentEditProfileBinding binding;

    public static EditProfileFragment newInstance() {
        return new EditProfileFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        EditProfileViewModel editProfileViewModel = new ViewModelProvider(this).get(EditProfileViewModel.class);

        binding = FragmentEditProfileBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        Button b1 = root.findViewById(R.id.btn_editar_perfil_guardar);
        b1.setOnClickListener(v -> {
            if(validar(v, root)){
                NavController navController = Navigation.findNavController(getParentFragment().getActivity(), R.id.nav_host_fragment_content_dashboard);
                navController.popBackStack();
            }
        });

        return root;
    }

    public boolean validar(View v, View root){
        TextInputLayout nombreLayout = root.findViewById(R.id.text_editar_perfil_nombre);
        TextInputLayout apellidoLayout = root.findViewById(R.id.text_editar_perfil_apellidos);
        TextInputLayout fecha_nacimientoLayout = root.findViewById(R.id.text_editar_perfil_fecha_nacimiento);
        TextInputLayout direccionLayout = root.findViewById(R.id.text_editar_perfil_direccion);
        TextInputLayout telefonoLayout = root.findViewById(R.id.text_editar_perfil_telefono);
        TextInputLayout correoLayout = root.findViewById(R.id.text_editar_perfil_correo);
        TextInputLayout passwordLayout = root.findViewById(R.id.text_editar_perfil_password);

        TextInputEditText editTextNombre = root.findViewById(R.id.input_editar_perfil_nombre);
        TextInputEditText editTextApellido = root.findViewById(R.id.input_editar_perfil_apellidos);
        TextInputEditText editTextFecha_nacimiento = root.findViewById(R.id.input_editar_perfil_fecha_nacimiento);
        TextInputEditText editTextDireccion = root.findViewById(R.id.input_editar_perfil_direccion);
        TextInputEditText editTextTelefono = root.findViewById(R.id.input_editar_perfil_telefono);
        TextInputEditText editTextCorreo = root.findViewById(R.id.input_editar_perfil_correo);
        TextInputEditText editTextPassword = root.findViewById(R.id.input_editar_perfil_password);

        boolean existeFocus = false;
        boolean existeError = false;

        if(Objects.isNull(editTextNombre.getText().toString()) || editTextNombre.getText().toString().isEmpty()){
            nombreLayout.setError("El nombre no puede estar vacio.");
            nombreLayout.requestFocus();
            existeFocus = true;
            existeError = true;
        }

        if(existeError){
            return false;
        }

        return true;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

}