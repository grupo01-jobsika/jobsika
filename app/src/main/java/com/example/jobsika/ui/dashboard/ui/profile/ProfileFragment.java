package com.example.jobsika.ui.dashboard.ui.profile;

import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.jobsika.R;
import com.example.jobsika.databinding.FragmentProfileBinding;
import com.example.jobsika.ui.auth.NuevaClaveActivity;

public class ProfileFragment extends Fragment {

    private FragmentProfileBinding binding;

    public static ProfileFragment newInstance() {
        return new ProfileFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        ProfileViewModel profileViewModel = new ViewModelProvider(this).get(ProfileViewModel.class);

        binding = FragmentProfileBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        Button b1 = root.findViewById(R.id.btn_editar_profile);
        b1.setOnClickListener(v -> {
            editar(v);
        });
        return root;
    }

    public void editar(View v){
        NavController navController = Navigation.findNavController(getParentFragment().getActivity(), R.id.nav_host_fragment_content_dashboard);
        navController.navigate(R.id.nav_edit_profile);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

}