package com.example.jobsika.ui.auth;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Toast;

import com.example.jobsika.R;
import com.example.jobsika.ui.dashboard.DashboardActivity;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_login);
    }

    public void recuperarClave(View v){
        startActivity(new Intent(this, RecuperarCuentaActivity.class));
    }
    public void registrar(View v){
        startActivity(new Intent(this, RegistrarActivity.class));
    }

    public void iniciarSesion(View v){
        startActivity(new Intent(this, DashboardActivity.class));
    }

}